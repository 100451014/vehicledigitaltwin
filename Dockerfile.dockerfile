# Use the official Python image as base image
FROM python:3.11.1

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the code from the local directory to the working directory in the container
COPY ./code /usr/src/app

# Install dependencies
RUN pip install --no-cache-dir requests math

# Run the Python script
CMD ["python", "VehicleDigitalTwin.py"]
