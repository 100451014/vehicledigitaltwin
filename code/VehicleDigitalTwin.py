import threading
import time
import json
import requests
import random

from math import radians, cos, sin, acos

global vehicleControlCommands
global currentRouteDetailedSteps

pending_routes = []

current_kms = None  #Not defined to avoid thread errors

luminosity = 0

distance_to_obstacle = 0

def decode_polyline(polyline_str):
    """Pass a Google Maps encoded polyline string; returns list of
lat/lon pairs."""
    index, lat, lng = 0, 0, 0
    coordinates = []
    changes = {'latitude': 0, 'longitude': 0}

    while index < len(polyline_str):
        for unit in ['latitude', 'longitude']:
            shift, result = 0, 0
            while True:
                byte = ord(polyline_str[index]) - 63
                index += 1
                result |= (byte & 0x1f) << shift
                shift += 5
                if not byte >= 0x20:
                    break
            if (result & 1):
                changes[unit] = ~(result >> 1)
            else:
                changes[unit] = (result >> 1)
        lat += changes['latitude']
        lng += changes['longitude']
        coordinates.append((lat / 100000.0, lng / 100000.0))
    return coordinates

def get_detailed_steps(steps):
    detailedStepsALL = []

    for step in steps:
        detailedSteps = []

    #Set speed in 100 scale
        stepSpeed = (step["distance"]["value"] / 1000) / (step["duration"]["value"] / 3600)

    #Set step distance
        stepDistance = step["distance"]["value"]

    #Set step time
        stepTime = step["duration"]["value"]

    #Set the movement of the steering wheel
        try:
            stepManeuver = step["maneuver"]
        except:
            stepManeuver = "Straight"

    #Set waypoints related to the step. 
    #Call method decode_polyline.
        substeps = decode_polyline(step["polyline"]["points"])

        for i in range(len(substeps) - 1):
            p1 = {"latitude": substeps[i][0], "longitude": substeps[i][1]}
            p2 = {"latitude": substeps[i + 1][0], "longitude": substeps[i + 1][1]}
            points_distance = distance(p1, p2) 
            if points_distance > 0.001:
                subStepDuration = points_distance / stepSpeed 
                new_detailed_step = {"Origin": p1, "Destination": p2, "Speed": stepSpeed, "Time": subStepDuration, "Distance": points_distance,
                                    "Maneuver": stepManeuver}
                detailedSteps.append(new_detailed_step)
        
        detailedStepsALL.append(detailedSteps)
    
    return detailedStepsALL

def getCommands(currentRouteDetailedSteps):
 
    steeringAngle: float = 90.0

    vehicleControlCommands = []
    index = 0

    #Commands 
    stepCommands = []
    for currentDetailedSteps in currentRouteDetailedSteps:
        for detailedStep in currentDetailedSteps:
            if (detailedStep["Maneuver"].upper() == "STRAIGHT" or detailedStep["Maneuver"].upper() == "RAMP_LEFT"
                    or detailedStep["Maneuver"].upper() == "RAMP_RIGHT" or detailedStep["Maneuver"].upper() == "MERGE"
                    or detailedStep["Maneuver"].upper() == "MANEUVER_UNSPECIFIED"):
                steeringAngle = 90.0
            if detailedStep["Maneuver"].upper() == "TURN_LEFT":
                steeringAngle = 45.0
            if detailedStep["Maneuver"].upper() == "UTURN_LEFT":
                steeringAngle = 0.0
            if detailedStep["Maneuver"].upper() == "TURN_SHARP_LEFT":
                steeringAngle = 15.0
            if detailedStep["Maneuver"].upper() == "TURN_SLIGHT_LEFT":
                steeringAngle = 60.0
            if detailedStep["Maneuver"].upper() == "TURN_RIGHT":
                steeringAngle = 135.0
            if detailedStep["Maneuver"].upper() == "UTURN_RIGHT":
                steeringAngle = 180.0
            if detailedStep["Maneuver"].upper() == "TURN_SHARP_RIGHT":
                steeringAngle = 105.0
            if detailedStep["Maneuver"].upper() == "TURN_SLIGHT_RIGHT":
                steeringAngle = 150.0
            newCommand = {"SteeringAngle": steeringAngle, "Speed": detailedStep["Speed"], "Time": detailedStep["Time"]} 
            stepCommands.append(newCommand)
        vehicleControlCommands.append(stepCommands)
        stepCommands = []
        index += 1
    return vehicleControlCommands

def distance(p1, p2):
    p1Latitude = p1["latitude"]
    p1Longitude = p1["longitude"]
    p2Latitude = p2["latitude"]
    p2Longitude = p2["longitude"]

    earth_radius = {"km": 6371.0087714, "mile": 3959} 
    result = earth_radius["km"] * acos(cos((radians(p1Latitude))) * cos(radians(p2Latitude)) * cos(radians(p2Longitude) - radians(p1Longitude)) + sin(
        radians(p1Latitude)) * sin(radians(p2Latitude))) 
    return result

def executeCommand(command, step):
    global current_steering
    global current_speed
    global current_position
    global current_kms

    # Actual distance
    current_kms = step["Distance"]

    current_steering = command["SteeringAngle"]
    current_speed = command["Speed"]
    time.sleep(command["Time"])
    current_position = step["Destination"]

def vehicle_stop():
    global current_steering
    global current_speed
    global current_leds
    global current_ldr
    global current_obstacle_distance

    vehicleControlCommands = []
    currentRouteDetailedSteps = []
    current_steering = 90.0
    current_speed = 0
    current_leds_str = '[{"Color": "White", "Intensity": 0.0, "Blinking": "False"},' \
                        '{"Color": "White", "Intensity": 0.0, "Blinking": "False"},' \
                        '{"Color": "Red",' '"Intensity": 0.0, "Blinking": "False"},' \
                        '{"Color": "Red", "Intensity": 0.0, "Blinking": "False"}]'
    current_leds = json.loads(current_leds_str)
    current_ldr = 0.0
    current_obstacle_distance = 0.0
    
def routes_manager(origin_address="Toronto", destination_address="Montreal"):

    google_maps_api_key = "AIzaSyBJ9hYoigiJBGjT5LfaauA2Vo1n1lthu9w"

    url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin_address + "&destination=" + \
        destination_address + "&key=" + google_maps_api_key 

    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)


    steps = response.json()["routes"][0]["legs"][0]["steps"] 

    # Collect steps and commands
    currentRouteDetailedSteps = get_detailed_steps(steps) 
    vehicleControlCommands = getCommands(currentRouteDetailedSteps)


    index = 0
    for step in currentRouteDetailedSteps[index]:
        index = 0
        if vehicleControlCommands: 
            for command in vehicleControlCommands[index]:
                executeCommand(command, step)
                if vehicleControlCommands: 
                    vehicleControlCommands.pop(0)
            
        index +=1 

    if len(pending_routes)>0:
        del pending_routes[0]

    elif len(pending_routes) == 0:
        vehicle_stop()
        time.sleep(10)
        threading.Event().wait(1)
    
def vehicle_controller():
    
    # Wait routes
    while pending_routes:
        routes_manager(pending_routes[0]['origin'], pending_routes[0]['destination'])
        
        # Time delay
        threading.Event().wait(10)
        pending_routes.pop(0)
        print("Ruta terminada")
        
    # Stop routes
    vehicle_stop()



def environment_simulator():
 
    global luminosity, distance_to_obstacle, current_kms

    
    while pending_routes:
        if current_kms > 0:
        
            luminosity += random.randint(-300, 300)
        
        if luminosity <= 0:
            luminosity = random.random(0.0, 3000.0)

        
        if distance_to_obstacle > 0:
            distance_to_obstacle += random.randint(-5, 5)
        else:
            luminosity = random.random(0.0, 50.0)

        if distance_to_obstacle < 10:
            print("Stop, there's an obstacle")

        elif distance_to_obstacle >= 10 and current_speed != 0.0:
            if vehicleControlCommands:
                current_speed = vehicleControlCommands[0]["Speed"]

        print(f"Luminosity: {luminosity}, Distance to obstacle: {distance_to_obstacle}")
        
        threading.Event().wait(1)

def led_controller():

    global luminosity

    while pending_routes:
        # Controla la iluminación basado en la luminosidad
        if luminosity < 500:
            print("Led goes ON")
        else:
            print("Led goes OFF")
        
        threading.Event().wait(1)
    

def routes_loader(origin, destination):

    route = {
        "origin": origin,
        "destination": destination,
        "commands": None  # Comandos se generarán cuando se procese la ruta
    }
    pending_routes.append(route)
    print(f"Route from {origin} to {destination} added")


    

if __name__ == "__main__":
    
    routes_loader("Toronto", "Montreal")
    
    
    threading.Thread(target=vehicle_controller, daemon=True).start()
    threading.Thread(target=environment_simulator, daemon=True).start()
    threading.Thread(target=led_controller, daemon=True).start()

        
    # Espera a los hilos a que terminen
    for thread in threading.enumerate():
        if thread is not threading.current_thread():
            thread.join()